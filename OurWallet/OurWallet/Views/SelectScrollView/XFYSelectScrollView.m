//
//  XFYSelectScrollView.m
//  IHKApp
//
//  Created by 小肥羊 on 15/8/7.
//  Copyright (c) 2015年 www.ihk.cn. All rights reserved.
//

#import "XFYSelectScrollView.h"

@implementation XFYSelectScrollView
#pragma mark - 初始化
- (void)initValue
{
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.33;
    self.layer.shadowOffset = CGSizeMake(0, 1.5);
    self.layer.shadowRadius = 4.0;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.scrollView];
    self.showCount = 4;     //默认为4
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self initValue];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        [self initValue];
    }
    return self;
}

#pragma mark - Set设置属性
- (void)setFrame:(CGRect)frame
{
    super.frame = frame;
    self.scrollView.frame = self.bounds;
    [self upViews];
}

- (void)setContents:(NSMutableArray *)contents
{
    _contents = contents;
    [self upViews];
}

- (void)upViews
{
    for(UIView *contentView in [self.scrollView subviews])
    {
        [contentView removeFromSuperview];
    }
    NSInteger showCount = self.contents.count < self.showCount ? self.contents.count : self.showCount;
    float buttonWidth = self.scrollView.bounds.size.width/showCount;
    float buttonHeight = self.scrollView.bounds.size.height;
    self.scrollView.contentSize = CGSizeMake(self.contents.count * buttonWidth, buttonHeight);
    for (NSInteger i=0; i<self.contents.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setFrame:(CGRect){CGPointMake(buttonWidth * i, 0), CGSizeMake(buttonWidth, buttonHeight)}];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        [button setTitle:self.contents[i] forState:UIControlStateNormal];
        [button setTag:ViewTag(i)];
        [button addTarget:self action:@selector(operation:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:button];
        if (i == 0) {
            [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        }
        if (i != (self.contents.count - 1)) {
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(buttonWidth, 12, 1, buttonHeight - 24)];
            [line setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            [button addSubview:line];
        }
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - 视图更新
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self upViews];
    [self.scrollView setFrame:(CGRect){self.scrollView.frame.origin, self.bounds.size}];
}

#pragma mark - 点击按钮
- (void)operation:(UIButton *)button
{
    for(UIButton *btn in [self.scrollView subviews])
    {
        if (ArrayIndex(btn.tag) < self.contents.count) {
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    if (self.delegate) {
        [self.delegate operation:self Button:button AtIndex:ArrayIndex(button.tag)];
    }
}

- (void)selectAtIndex:(NSInteger)index
{
    for(UIButton *btn in [self.scrollView subviews])
    {
        if (ArrayIndex(btn.tag) < self.contents.count) {
            if (ArrayIndex(btn.tag) == index) {
                [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                float maxOffX = self.scrollView.contentSize.width - btn.frame.size.width * self.showCount;
                float contentOffX = btn.frame.size.width * index;
                if (contentOffX > maxOffX) {
                    contentOffX = maxOffX;
                }
                [self.scrollView setContentOffset:CGPointMake(contentOffX, 0) animated:YES];
            } else {
                [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            }
        }
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
