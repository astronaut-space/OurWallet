//
//  XFYSelectScrollView.h
//  IHKApp
//
//  Created by 小肥羊 on 15/8/7.
//  Copyright (c) 2015年 www.ihk.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XFYSelectScrollView;
@protocol XFYSelectScrollViewDelegate <NSObject>
@required       //必须实现的代理
- (void)operation:(XFYSelectScrollView *)selectView Button:(UIButton *)button AtIndex:(NSInteger)index;
@end

@interface XFYSelectScrollView : UIView

@property (nonatomic, strong) UIScrollView *scrollView;     /**< 滚动图 */
@property (nonatomic, strong) NSMutableArray *contents;     /**< 内容数组 */
@property (nonatomic) NSInteger showCount;                  /**< 显示单位 */
@property (nonatomic, weak) id <XFYSelectScrollViewDelegate> delegate;      /**< 代理 */

- (void)selectAtIndex:(NSInteger)index;

@end
