//
//  CashView.h
//  OurWallet
//
//  Created by 小肥羊 on 15/12/31.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLSwipeableView.h"
#import "CardView.h"

@class CashView;
@protocol CashDelegate <NSObject>

- (void)changeMoney:(CashView *)cashView;

@end

@interface CashView : UIView <ZLSwipeableViewDataSource, ZLSwipeableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *cashLabel;
@property (weak, nonatomic) IBOutlet ZLSwipeableView *moneyView;

@property (weak, nonatomic) IBOutlet UILabel *allCashRemark;
@property (weak, nonatomic) IBOutlet UILabel *allCashLabel;

@property (nonatomic, assign) id <CashDelegate> delegate;      /**< 钱代理 */

@end
