//
//  CashView.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/31.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "CashView.h"

@implementation CashView


- (id)initWithFrame:(CGRect)frame
{
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"CashView" owner:nil options:nil];
    self = [nibs lastObject];
    if (self) {
        self.frame = frame;

        self.moneyView.numberOfActiveViews = 2;
        self.moneyView.allowedDirection = ZLSwipeableViewDirectionRight;
        // Required Data Source
        self.moneyView.dataSource = self;
        // Optional Delegate
        self.moneyView.delegate = self;
        self.moneyView.translatesAutoresizingMaskIntoConstraints = NO;
    } else {
        self = [super initWithFrame:frame];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    [self.moneyView loadViewsIfNeeded];
}


#pragma mark - ZLSwipeableViewDelegate
- (void)swipeableView:(ZLSwipeableView *)swipeableView
         didSwipeView:(UIView *)view
          inDirection:(ZLSwipeableViewDirection)direction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeMoney:)]) {
        [self.delegate changeMoney:self];
    }
//    NSLog(@"did swipe in direction: %zd", direction);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView didCancelSwipe:(UIView *)view {
//    NSLog(@"did cancel swipe");
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
  didStartSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
//    NSLog(@"did start swiping at location: x %f, y %f", location.x, location.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
          swipingView:(UIView *)view
           atLocation:(CGPoint)location
          translation:(CGPoint)translation {
//    NSLog(@"swiping at location: x %f, y %f, translation: x %f, y %f", location.x, location.y, translation.x, translation.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
    didEndSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
//    NSLog(@"did end swiping at location: x %f, y %f", location.x, location.y);
}


#pragma mark - ZLSwipeableViewDataSource
- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    
    CardView *view = [[CardView alloc] initWithFrame:swipeableView.bounds];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
