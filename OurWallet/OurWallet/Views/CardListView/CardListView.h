//
//  CardListView.h
//  OurWallet
//
//  Created by 小肥羊 on 16/1/5.
//  Copyright © 2016年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardCell.h"

@interface CardListView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@property (strong ,nonatomic) NSArray *cardArray;

@property (nonatomic, assign) id <CardCellDelegate> delegate;       /**< 代理 */

@end
