//
//  CardCell.m
//  OurWallet
//
//  Created by 小肥羊 on 16/1/5.
//  Copyright © 2016年 小肥羊. All rights reserved.
//

#import "CardCell.h"

@implementation CardCell

- (void)awakeFromNib {
    // Initialization code
    self.cardView.numberOfActiveViews = 1;
    self.cardView.allowedDirection = ZLSwipeableViewDirectionUp;
    // Required Data Source
    self.cardView.dataSource = self;
    // Optional Delegate
    self.cardView.delegate = self;
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    [self.cardView loadViewsIfNeeded];
}


#pragma mark - ZLSwipeableViewDelegate
- (void)swipeableView:(ZLSwipeableView *)swipeableView
         didSwipeView:(UIView *)view
          inDirection:(ZLSwipeableViewDirection)direction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(usingCard:IndexPath:)]) {
        [self.delegate usingCard:self IndexPath:self.cellIndex];
    }
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView didCancelSwipe:(UIView *)view {
    //    NSLog(@"did cancel swipe");
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
  didStartSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    //    NSLog(@"did start swiping at location: x %f, y %f", location.x, location.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
          swipingView:(UIView *)view
           atLocation:(CGPoint)location
          translation:(CGPoint)translation {
    //    NSLog(@"swiping at location: x %f, y %f, translation: x %f, y %f", location.x, location.y, translation.x, translation.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
    didEndSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    //    NSLog(@"did end swiping at location: x %f, y %f", location.x, location.y);
}


#pragma mark - ZLSwipeableViewDataSource
- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    
    CardView *card = [[CardView alloc] initWithFrame:swipeableView.bounds];
    [card setBackgroundColor:[UIColor whiteColor]];
    return card;
}



@end
