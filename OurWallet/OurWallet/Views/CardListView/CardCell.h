//
//  CardCell.h
//  OurWallet
//
//  Created by 小肥羊 on 16/1/5.
//  Copyright © 2016年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLSwipeableView.h"
#import "CardView.h"

@class CardCell;
@protocol CardCellDelegate <NSObject>
/*! @brief cell操作 */
- (void)usingCard:(CardCell *)cell IndexPath:(NSIndexPath *)indexPath;

@end

@interface CardCell : UITableViewCell <ZLSwipeableViewDataSource, ZLSwipeableViewDelegate>

@property (weak, nonatomic) IBOutlet ZLSwipeableView *cardView;
@property (weak, nonatomic) IBOutlet UILabel *cardNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (strong, nonatomic) NSIndexPath *cellIndex;               /**< cell位置标示 */
@property (nonatomic, assign) id <CardCellDelegate> delegate;       /**< 代理 */

@property (copy, nonatomic) NSString *cardName;

@end
