//
//  WalletRecordCell.h
//  MyWallet
//
//  Created by 小肥羊 on 15/8/15.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletRecordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *moneyFrom;
@property (weak, nonatomic) IBOutlet UILabel *moneyFor;
@property (weak, nonatomic) IBOutlet UILabel *money;
@property (weak, nonatomic) IBOutlet UILabel *remarks;
@property (weak, nonatomic) IBOutlet UILabel *recordDate;
@property (weak, nonatomic) IBOutlet UILabel *byUser;
@property (weak, nonatomic) IBOutlet UIImageView *recordImage;

- (void)moneyText:(NSString *)money Consume:(BOOL)consume;

@end
