//
//  FundStatisticsCell.m
//  MyWallet
//
//  Created by 小肥羊 on 15/8/27.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import "FundStatisticsCell.h"

@implementation FundStatisticsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 公共方法
- (void)moneyTextWithPay:(NSString *)pay Income:(NSString *)income
{
    [self.payMoney setText:[NSString stringWithFormat:@"-%@",pay]];
    [self.incomeMoney setText:[NSString stringWithFormat:@"+%@",income]];
    float sum = [income floatValue] - [pay floatValue];
    if (sum < 0) {
        [self.summary setText:[NSString stringWithFormat:@"%@",@(sum)]];
        [self.summary setTextColor:[UIColor greenColor]];
    } else {
        [self.summary setText:[NSString stringWithFormat:@"+%@",@(sum)]];
        [self.summary setTextColor:[UIColor redColor]];
    }
}

@end
