//
//  FundStatisticsCell.h
//  MyWallet
//
//  Created by 小肥羊 on 15/8/27.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundStatisticsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *incomeMoney;
@property (weak, nonatomic) IBOutlet UILabel *payMoney;
@property (weak, nonatomic) IBOutlet UILabel *summary;
@property (weak, nonatomic) IBOutlet UILabel *statisticsDate;

- (void)moneyTextWithPay:(NSString *)pay Income:(NSString *)income;

@end
