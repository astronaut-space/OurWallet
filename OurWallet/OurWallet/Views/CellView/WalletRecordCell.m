//
//  WalletRecordCell.m
//  MyWallet
//
//  Created by 小肥羊 on 15/8/15.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import "WalletRecordCell.h"

@implementation WalletRecordCell

- (void)awakeFromNib {
    // Initialization code
    [self.moneyFrom setText:@""];
    [self.moneyFor setText:@""];
    [self.remarks setText:@""];
    [self.recordDate setText:@""];
    [self.money setText:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - 公共方法
- (void)moneyText:(NSString *)money Consume:(BOOL)consume
{
    if (consume) {
        [self.money setTextColor:[UIColor greenColor]];
        [self.money setText:[NSString stringWithFormat:@"-%@",money]];
    } else {
        [self.money setTextColor:[UIColor redColor]];
        [self.money setText:[NSString stringWithFormat:@"+%@",money]];
    }
}

@end
