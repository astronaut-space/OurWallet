//
//  SelectDateView.m
//  MyWallet
//
//  Created by 小肥羊 on 15/7/17.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import "SelectDateView.h"

@implementation SelectDateView

- (id)initWithDelegate:(id <SelectDateDelegate>)delegate
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"SelectDateView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.delegate = delegate;
        [self.layer setCornerRadius:kNomalRadius];
        [self.layer setMasksToBounds:YES];
    }
    return self;
}
- (IBAction)cancelDate:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(cancelSelect:)]) {
        [self.delegate cancelSelect:self];
    }
}
- (IBAction)selectDate:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(selectDate:)]) {
        [self.delegate selectDate:self];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
