//
//  SelectDateView.h
//  MyWallet
//
//  Created by 小肥羊 on 15/7/17.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelectDateView;
/*! @brief 代理 */
@protocol SelectDateDelegate <NSObject>

@optional
- (void)pickerDidChaneDate:(SelectDateView *)picker;
- (void)selectDate:(SelectDateView *)picker;
- (void)cancelSelect:(SelectDateView *)picker;
@end

@interface SelectDateView : UIView

@property (weak, nonatomic) id <SelectDateDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;


- (id)initWithDelegate:(id <SelectDateDelegate>)delegate;

@end
