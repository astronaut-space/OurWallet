//
//  ActionView.h
//  MyWallet
//
//  Created by 小肥羊 on 15/7/17.
//  Copyright (c) 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>

#pragma mark - 宏定义

/*! @brief 弹出框样式 */
typedef NS_ENUM(NSInteger, MenuViewStyle){
    MenuViewStyleLight = 0,     /**< 浅色背景，深色字体 */
    MenuViewStyleDark           /**< 深色背景，浅色字体 */
};

/*! @brief Pop位置 */
typedef NS_ENUM(NSInteger, PopViewDirectionStyle){
    PopViewDirectionStyleCenter = 0,    /**< 居中 */
    PopViewDirectionStyleUp,            /**< 置顶 */
    PopViewDirectionStyleDown,          /**< 置底 */
    PopViewDirectionStyleDIY            /**< 自定义 */
};

typedef void(^ViewActionHandler)(NSInteger index);

@interface ActionView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableArray *views;                /**< 视图组 */
@property (nonatomic, strong) CAAnimation *showViewAnimation;       /**< 显示动画 */
@property (nonatomic, strong) CAAnimation *dismissViewAnimation;    /**< 消失动画 */
@property (nonatomic, strong) CAAnimation *dimingAnimation;         /**< 动画 */
@property (nonatomic, strong) CAAnimation *lightingAnimation;       /**< 动画 */
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;   /**< 点击背景取消 */

/**
 *  弹出框样式
 */
@property (nonatomic, assign) MenuViewStyle style;

/*! @brief 获取单例 */
+ (ActionView *)sharedActionView;

/*! @brief 显示视图 */
+ (void)showView:(UIView *)view;
+ (void)showView:(UIView *)view direction:(PopViewDirectionStyle)directionStyle;

/*! @brief 关闭视图 */
+ (void)closeView:(UIView *)view;


@end

