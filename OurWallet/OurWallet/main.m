//
//  main.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        [Bmob registerWithAppKey:kBmob_ApplicationID];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
