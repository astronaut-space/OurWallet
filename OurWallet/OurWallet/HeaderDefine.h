//
//  HeaderDefine.h
//  SDKSample
//
//  Created by 小肥羊 on 15/12/15.
//  Copyright © 2015年 Zhu‘s Game House. All rights reserved.
//

#ifndef HeaderDefine_h
#define HeaderDefine_h


#define iDeviceVersion  [[UIDevice currentDevice].systemVersion floatValue] //系统版本


#pragma mark - UI相关
#define uScreenWidth    [UIScreen mainScreen].bounds.size.width             //设备宽度
#define uScreenHeight   [UIScreen mainScreen].bounds.size.height            //设备高度

#define uNavbarHeight   ((iDeviceVersion>=7.0)? 64 :44 )                    //导航栏高度
#define uIOS7DELTA      ((iDeviceVersion>=7.0)? 20 :0 )                     //状态栏高度

#define kNomalRadius    (5.0f)

#define ViewTag(x)      ((int)x + 100)
#define ArrayIndex(x)   ((int)x - 100)


#pragma mark - Bmob Key值
#define kBmob_ApplicationID     (@"b3b10d7de7dc46695a6e596e26b94031")       //Application ID
#define kBmob_SecretKey         (@"6c4966d25115cd0e")                       //Secret Key


#pragma mark - 色值
#define KTableViewSeparatorColor ([UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0])


#pragma mark - 方法
#define tDictValue(__X__)        [__X__ isKindOfClass:[NSNull class]] ? @"" : [NSString stringWithFormat:@"%@", (__X__)]     //防止信息为nil


#pragma mark -记录参数
/*
 说明
 Macro_MoneyFromWallet 数据变化来源
 Macro_MoneyForSave 来源参数
 Macro_MoneyForDraw 消费参数
 Macro_MoneyForChange 钱包之间的变化,不计算在来源于消费中
 */
#define Macro_MoneyFromWallet   (@"现金")
#define Macro_MoneyForSave      (@[@"莫名奇妙",@"工资",@"家庭福利",@"天降横财",@"借钱"])
#define Macro_MoneyForDraw      (@[@"莫名奇妙",@"饮食",@"娱乐",@"装扮",@"房租",@"交通",@"日常用品",@"还钱"])
#define Macro_MoneyForChange    (@"换个地方")

#endif /* HeaderDefine_h */
