//
//  RegistViewController.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/29.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "RegistViewController.h"

@interface RegistViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userNameText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;


@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)clickRegist:(id)sender {
    [self.view endEditing:YES];
    //使用SDK注册
    [self addProgressHud];
    BmobUser *bUser = [[BmobUser alloc] init];
    bUser.username = self.userNameText.text;
    [bUser setPassword:self.passWordText.text];
    [bUser signUpInBackgroundWithBlock:^ (BOOL isSuccessful, NSError *error){
        [self removeProgressHud];
        if (isSuccessful){
            [self.navigationController popViewControllerAnimated:YES];
            [MBProgressHUD showSuccessWithText:@"注册成功"];
        } else {
            NSLog(@"object %@",error);
            [MBProgressHUD showErrorWithText:error.userInfo[@"error"]];
        }
    }];
}
- (IBAction)clickBackground:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark 键盘代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.userNameText]) {
        [self.passWordText becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
