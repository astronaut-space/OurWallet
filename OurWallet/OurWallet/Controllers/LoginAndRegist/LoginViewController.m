//
//  LoginViewController.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController () <BmobEventDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userNameText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    BmobEvent *bmobEvent = [BmobEvent defaultBmobEvent];
    //设置代理
    bmobEvent.delegate = self;
    //启动连接
    [bmobEvent start];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    BmobUser *bUser = [BmobUser getCurrentUser];
    if (bUser) {
        [self.userNameText setText:bUser.username];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([UserInfoModel sharedManager].login) {
        [self.navigationController performSegueWithIdentifier:@"TurnMain" sender:self];
    }
}

- (IBAction)clickLogin:(id)sender {
    [self.view endEditing:YES];
    [self addProgressHud];
    [BmobUser loginInbackgroundWithAccount:self.userNameText.text
                               andPassword:self.passWordText.text
                                     block:^(BmobUser *user, NSError *error) {
                                         [self removeProgressHud];
                                         if (user){
                                             USERINFO.objectId = user.objectId;
                                             USERINFO.login = YES;
                                             [self.navigationController performSegueWithIdentifier:@"TurnMain" sender:self];
                                         } else {
                                             [MBProgressHUD showErrorWithText:error.userInfo[@"error"]];
                                         }
                                     }];
}
- (IBAction)clickBackground:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark 键盘代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.userNameText]) {
        [self.passWordText becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 监听
- (void)bmobEventCanStartListen:(BmobEvent *)event
{
    //监听表更新
    [event listenTableChange:BmobActionTypeUpdateTable tableName:@"_User"];
    [event listenTableChange:BmobActionTypeUpdateTable tableName:@"Card"];
}
//接收到得数据
- (void)bmobEvent:(BmobEvent *)event didReceiveMessage:(NSString *)message
{
    NSData *jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
    } else {
        if ([dict[@"tableName"] isEqualToString:@"_User"]) {
            [USERINFO upUserInfo];
        } else if ([dict[@"tableName"] isEqualToString:@"Card"]) {
            [USERINFO upCard];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
