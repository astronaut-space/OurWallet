//
//  CardOperationViewController.m
//  OurWallet
//
//  Created by 小肥羊 on 16/1/5.
//  Copyright © 2016年 小肥羊. All rights reserved.
//

#import "CardOperationViewController.h"
#import "XFYSelectScrollView.h"
#import "SelectDateView.h"
#import "ActionView.h"
#import "WalletRecordCell.h"

static NSString *WalletRecordCellIdentifier = @"WalletRecordCell";

@interface CardOperationViewController () <SelectDateDelegate, XFYSelectScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancellationButton;
@property (weak, nonatomic) IBOutlet UITextField *cardNameText;
@property (weak, nonatomic) IBOutlet UITextField *moneyText;

@property (weak, nonatomic) IBOutlet UIView *recordView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UITextField *changeMoneyText;
@property (weak, nonatomic) IBOutlet XFYSelectScrollView *moneyForSelectView;
@property (weak, nonatomic) IBOutlet UIButton *changeDateButton;
@property (weak, nonatomic) IBOutlet UITextField *remarkText;
@property (weak, nonatomic) IBOutlet UITableView *recordTableView;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

@property (nonatomic, strong) SelectDateView *selectDateView;   /**< 选择时间视图 */
@property (nonatomic, strong) NSString *moneyFor;               /**< 消费源 */
@property (nonatomic, strong) NSDate *selectDate;               /**< 时间 */
@property (nonatomic, strong) NSData *imageData;                /**< 图片 */
@property (nonatomic, strong) NSArray *recordArray;             /**< 记录 */
@property (nonatomic, strong) NSDictionary *cardInfo;
@property (nonatomic, getter=isCreate) BOOL create;
@property (nonatomic, getter=isConsume) BOOL consume;

@end

@implementation CardOperationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSInteger cardIndex = [[self.receiveDict objectForKey:@"cardIndex"] integerValue];
    if (cardIndex < USERINFO.cardList.count) {
        self.cardInfo = USERINFO.cardList[cardIndex];
        [self.cardNameText setText:self.cardInfo[@"cardName"]];
        [self.moneyText setText:[self.cardInfo[@"money"] stringValue]];
        [self.cardNameText setEnabled:NO];
        [self.moneyText setEnabled:NO];
        self.create = NO;
        self.moneyForSelectView.delegate = self;
        self.selectDate = [NSDate date];
        [self.changeDateButton.layer setCornerRadius:kNomalRadius];
        [self.changeDateButton.layer setBorderWidth:0.8];
        [self.changeDateButton.layer setBorderColor:KTableViewSeparatorColor.CGColor];
        self.recordTableView.delegate = self;
        self.recordTableView.dataSource = self;
        [self.recordTableView registerNib:[UINib nibWithNibName:WalletRecordCellIdentifier bundle:nil] forCellReuseIdentifier:WalletRecordCellIdentifier];
        
        self.recordArray = [[NSArray alloc] init];
        __weak typeof(self) _WeakSelf = self;
        BmobQuery *bquery = [BmobQuery queryWithClassName:@"ChangeRecord"];
        [bquery whereKey:@"fromUserId" equalTo:USERINFO.objectId];
        [bquery whereKey:@"useFrom" equalTo:self.cardInfo[@"objectId"]];
        [bquery orderByDescending:@"recordTime"];
        bquery.limit = 10;
        [bquery calcInBackgroundWithBlock:^(NSArray *array,NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _WeakSelf.recordArray = array;
                [_WeakSelf.recordTableView reloadData];
            });
        }];
    } else {
        self.create = YES;
    }
    self.consume = YES;
}

#pragma mark - 惰性初始化
- (SelectDateView *)selectDateView
{
    if (!_selectDateView) {
        _selectDateView = [[SelectDateView alloc] initWithDelegate:self];
    }
    return _selectDateView;
}

#pragma mark - 属性设置
- (void)setCreate:(BOOL)create
{
    _create = create;
    if (_create) {
        [self setTitle:@"激活卡"];
        [self.cancellationButton setTitle:@""];
    } else {
        [self setTitle:@"刷卡"];
        [self.cancellationButton setTitle:@"注销卡"];
    }
    [self.recordView setHidden:_create];
}
- (void)setConsume:(BOOL)consume
{
    _consume = consume;
    if (self.create) {
        [self setTitle:@"激活卡"];
        [self.cancellationButton setTitle:@""];
    } else {
        if (_consume) {
            [self setTitle:@"刷卡"];
            [self.typeLabel setText:@"消费金额:"];
            self.moneyForSelectView.contents = [[NSMutableArray alloc] initWithArray:Macro_MoneyForDraw];
            self.moneyFor = Macro_MoneyForDraw[0];
        } else {
            [self setTitle:@"入帐"];
            [self.typeLabel setText:@"入帐金额:"];
            self.moneyForSelectView.contents = [[NSMutableArray alloc] initWithArray:Macro_MoneyForSave];
            self.moneyFor = Macro_MoneyForDraw[0];
        }
    }
}
- (void)setSelectDate:(NSDate *)selectDate
{
    _selectDate = selectDate;
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSString *locationString = [dateformatter stringFromDate:_selectDate];
    [self.changeDateButton setTitle:locationString forState:UIControlStateNormal];
}

#pragma mark - 操作
- (IBAction)clickBackground:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)changeType:(UISwitch *)sender {
    self.consume = sender.isOn;
}
- (IBAction)changeDate:(id)sender {
    [self.view endEditing:YES];
    [ActionView showView:self.selectDateView direction:PopViewDirectionStyleCenter];
}
- (IBAction)addImage:(id)sender {
    UIActionSheet *sheet;
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        sheet  = [[UIActionSheet alloc] initWithTitle:@"设置照片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍摄新相片",@"从照片库选择", nil];
    }
    else
    {
        sheet = [[UIActionSheet alloc] initWithTitle:@"设置照片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从照片库选择", nil];
    }
    [sheet showInView:self.view];
}


/*! @brief 确认 */
- (IBAction)confirm:(id)sender {
    __weak typeof(self) _WeakSelf = self;
    if (self.isCreate) {
        [self addProgressHud];
        BmobObject *cardScore = [BmobObject objectWithClassName:@"Card"];
        [cardScore setObject:self.cardNameText.text forKey:@"cardName"];
        [cardScore setObject:USERINFO.objectId forKey:@"userId"];
        [cardScore setObject:[NSNumber numberWithInteger:[self.moneyText.text integerValue]] forKey:@"money"];
        //异步保存到服务器
        [cardScore saveInBackgroundWithResultBlock:^(BOOL isSuccessful, NSError *error) {
            [_WeakSelf removeProgressHud];
            if (isSuccessful) {
                [MBProgressHUD showSuccessWithText:@"激活成功"];
                [_WeakSelf.navigationController popViewControllerAnimated:YES];
            } else {
                [MBProgressHUD showErrorWithText:error.userInfo[@"error"]];
            }
        }];
    } else {
        [self addProgressHud];
        if (self.imageData) {
            BmobFile *bmobFile = [[BmobFile alloc] initWithFileName:@"recordImage.jpg"  withFileData:self.imageData];
            [bmobFile saveInBackground:^(BOOL isSuccessful, NSError *error) {
                [_WeakSelf removeProgressHud];
                if (isSuccessful) {
                    [_WeakSelf upRecordWithBmobFile:bmobFile];
                } else {
                    [MBProgressHUD showErrorWithText:error.userInfo[@"error"]];
                }
            } withProgressBlock:^(float progress) {
                NSLog(@"%f",progress);
            }];
        } else {
            [_WeakSelf upRecordWithBmobFile:nil];
        }
    }

}

- (void)upRecordWithBmobFile:(BmobFile *)image
{
    __weak typeof(self) _WeakSelf = self;
    NSInteger changeMoney = [self.changeMoneyText.text integerValue];
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"Card"];
    [bquery getObjectInBackgroundWithId:self.cardInfo[@"objectId"] block:^(BmobObject *object,NSError *error){
        if (!error) {
            if (object) {
                BmobObject *card = [BmobObject objectWithoutDatatWithClassName:object.className objectId:object.objectId];
                if (_WeakSelf.isConsume) {
                    [card decrementKey:@"money" byAmount:changeMoney];
                } else {
                    [card incrementKey:@"money" byAmount:changeMoney];
                }
                [card updateInBackgroundWithResultBlock:^(BOOL isSuccessful, NSError *error) {
                    [_WeakSelf removeProgressHud];
                    if (isSuccessful) {
                        [MBProgressHUD showSuccessWithText:@"更新成功"];
                        [WalletNetwork changeRecordWithUseFor:_WeakSelf.moneyFor UseFrom:_WeakSelf.cardInfo[@"objectId"] Money:changeMoney Consume:_WeakSelf.consume Remark:_WeakSelf.remarkText.text RecordTime:_WeakSelf.selectDate RecordImage:image];
                        [_WeakSelf.navigationController popViewControllerAnimated:YES];
                    } else {
                        [MBProgressHUD showErrorWithText:error.userInfo[@"error"]];
                    }
                }];
            }
        } else {
            [_WeakSelf removeProgressHud];
            [MBProgressHUD showErrorWithText:@"更新失败"];
        }
    }];
}

/*! @brief 注销 */
- (IBAction)cancellation:(id)sender {
    if (self.isCreate) {
        NSLog(@"无操作");
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定要注销此卡？" preferredStyle:UIAlertControllerStyleAlert];
        // Create the actions.
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            __weak typeof(self) _WeakSelf = self;
            [_WeakSelf addProgressHud];
            BmobObject *bmobObject = [BmobObject objectWithoutDatatWithClassName:@"Card"  objectId:_WeakSelf.cardInfo[@"objectId"]];
            [bmobObject deleteInBackgroundWithBlock:^(BOOL isSuccessful, NSError *error) {
                [_WeakSelf removeProgressHud];
                if (isSuccessful) {
                    [MBProgressHUD showSuccessWithText:@"注销成功"];
                    [_WeakSelf.navigationController popViewControllerAnimated:YES];
                } else if (error){
                    [MBProgressHUD showErrorWithText:error.userInfo[@"error"]];
                } else {
                    [MBProgressHUD showErrorWithText:@"注销失败"];
                }
            }];
        }];
        UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSLog(@"取消");
        }];
        // Add the actions.
        [alertController addAction:confirmAction];
        [alertController addAction:cancleAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


#pragma mark 键盘代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.cardNameText]) {
        [self.moneyText becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 列表显示代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WalletRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:WalletRecordCellIdentifier];
    BmobObject *data = self.recordArray[indexPath.row];
    NSDictionary *recordDtate = [data objectForKey:@"recordTime"];
    [cell.moneyFrom setText:[data objectForKey:@"useFor"]];
    [cell.remarks setText:[data objectForKey:@"remark"]];
    [cell.recordDate setText:[recordDtate objectForKey:@"iso"]];
    [cell moneyText:[[data objectForKey:@"money"] stringValue] Consume:[[data objectForKey:@"consume"] boolValue]];
    if ([data objectForKey:@"recordImage"]) {
        BmobFile *file = (BmobFile *)[data objectForKey:@"recordImage"];
        [cell.recordImage sd_setImageWithURL:[WalletNetwork imageURLWithString:file.url]];
    }
    return cell;
}

#pragma mark - XFYSelectScrollView代理
- (void)operation:(XFYSelectScrollView *)selectView Button:(UIButton *)button AtIndex:(NSInteger)index
{
    [self.view endEditing:YES];
    self.moneyFor = button.titleLabel.text;
}

#pragma mark - 时间选择代理
- (void)selectDate:(SelectDateView *)picker
{
    self.selectDate = picker.datePicker.date;
    [ActionView closeView:picker];
}
- (void)cancelSelect:(SelectDateView *)picker
{
    [ActionView closeView:picker];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUInteger sourceType = 0;
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        switch (buttonIndex) {
            case 0: // 拍摄新相片
                sourceType = UIImagePickerControllerSourceTypeCamera;
                break;
            case 1: // 从照片库选择
                sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                break;
            case 2: // 取消
                return;
        }
    }
    else {
        if (buttonIndex == 0) {
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
        else {  // 取消
            return;
        }
    }
    // 跳转到相机或相册页面
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = sourceType;
    [self presentViewController:imagePickerController animated:NO  completion:nil];
}

#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imageButton setImage:image forState:UIControlStateNormal];
    self.imageData = UIImageJPEGRepresentation(image, 0.2);
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.imageButton setImage:image forState:UIControlStateNormal];
    }];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
