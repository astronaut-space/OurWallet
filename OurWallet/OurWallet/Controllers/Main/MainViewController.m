//
//  MainViewController.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "MainViewController.h"
#import "CashView.h"
#import "CardListView.h"

@interface MainViewController () <CashDelegate, CardCellDelegate>

@property (strong, nonatomic) CashView *cashView;
@property (strong, nonatomic) CardListView *cardListView;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
    [USERINFO upUserInfo];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showUI) name:nUserInfoChange object:nil];
}

- (void)createUI
{
    self.cardListView = [[CardListView alloc] initWithFrame:CGRectMake(uScreenWidth/2, 0, uScreenWidth/2, uScreenHeight)];
    self.cardListView.delegate = self;
    [self.view addSubview:self.cardListView];
    
    self.cashView = [[CashView alloc] initWithFrame:CGRectMake(0, 0, uScreenWidth/2, uScreenHeight)];
    self.cashView.delegate = self;
    [self.view addSubview:self.cashView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)dealloc
{
    NSLog(@"removeObserver");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nUserInfoChange object:nil];
}

#pragma mark - 显示
- (void)showUI
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (USERINFO.cash) {
            [self.cashView.cashLabel setText:[USERINFO.cash stringValue]];
        }
        if (USERINFO.familyName.length > 0) {
            [self.cashView.allCashRemark setHidden:NO];
            [self.cashView.allCashLabel setHidden:NO];
            if (USERINFO.allCash) {
                [self.cashView.allCashLabel setText:[USERINFO.allCash stringValue]];
            } else {
                [self.cashView.allCashLabel setText:@"0.00"];
            }
        } else {
            [self.cashView.allCashRemark setHidden:YES];
            [self.cashView.allCashLabel setHidden:YES];
        }
        self.cardListView.cardArray = USERINFO.cardList;
    });
}



#pragma mark - 钱代理
- (void)changeMoney:(CashView *)cashView
{
    [self performSegueWithIdentifier:@"ChangeCash" sender:self];
}

#pragma mark - 卡代理
- (void)usingCard:(CardCell *)cell IndexPath:(NSIndexPath *)indexPath
{
    self.passDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"cardIndex":[NSString stringWithFormat:@"%@", @(indexPath.row)]}];
    [self performSegueWithIdentifier:@"CardOperation" sender:self];
}


#pragma mark - 传参
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    BaseViewController *send = segue.destinationViewController;
    send.receiveDict = self.passDict;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
