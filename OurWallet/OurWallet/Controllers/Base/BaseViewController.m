//
//  BaseViewController.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property (strong, nonatomic) MBProgressHUD *myProgressHud;

@end


@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)addProgressHud
{
    if (!self.myProgressHud) {
        self.myProgressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}
- (void)addProgressHud:(NSString *)message
{
    if (!self.myProgressHud)
    {
        self.myProgressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    self.myProgressHud.labelText = message;
}
- (void)removeProgressHud
{
    if (self.myProgressHud) {
        [self.myProgressHud removeFromSuperview];
        self.myProgressHud = nil;
    }
}

@end
