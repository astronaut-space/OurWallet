//
//  BaseViewController.h
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) NSMutableDictionary *receiveDict;     /**< 接收参数 */
@property (nonatomic, strong) NSMutableDictionary *passDict;        /**< 传递参数 */

- (void)addProgressHud;
- (void)addProgressHud:(NSString *)message;
- (void)removeProgressHud;

@end
