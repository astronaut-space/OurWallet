//
//  UserInfoModel.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#define Userinfo [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"WalletUserinfo.data"]

#import "UserInfoModel.h"


@interface UserInfoModel ()



@end


@implementation UserInfoModel
#pragma mark - 单例
+ (UserInfoModel *)sharedManager
{
    static UserInfoModel *sharedAccountManagerInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedAccountManagerInstance = [NSKeyedUnarchiver unarchiveObjectWithFile:Userinfo];
        if (!sharedAccountManagerInstance) {
            sharedAccountManagerInstance = [[UserInfoModel alloc] init];
            sharedAccountManagerInstance.login = NO;
        }
    });
    return sharedAccountManagerInstance;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        _objectId = [decoder decodeObjectForKey:@"objectId"];
        _sessionToken = [decoder decodeObjectForKey:@"sessionToken"];
        _username = [decoder decodeObjectForKey:@"username"];
        _familyName = [decoder decodeObjectForKey:@"familyName"];
        _cash = [decoder decodeObjectForKey:@"cash"];
        _login = [decoder decodeBoolForKey:@"login"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.objectId forKey:@"objectId"];
    [encoder encodeObject:self.sessionToken forKey:@"sessionToken"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.familyName forKey:@"familyName"];
    [encoder encodeObject:self.cash forKey:@"cash"];
    [encoder encodeBool:self.login forKey:@"login"];
}

/*! @brief 保存用户信息 */
- (void)saveUserinfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:nUserInfoChange object:self];
    [NSKeyedArchiver archiveRootObject:self toFile:Userinfo];
}



#pragma mark - Set属性 自动保存
- (void)setUsername:(NSString *)username
{
    _username = username;
    [self saveUserinfo];
}
- (void)setFamilyName:(NSString *)familyName
{
    _familyName = familyName;
    if (_familyName.length <= 0) {
        self.familyBuddy = [[NSArray alloc] init];
    }
    [self saveUserinfo];
}
- (void)setLogin:(BOOL)login
{
    _login = login;
    [[NSNotificationCenter defaultCenter] postNotificationName:nLoginChange object:self];
    [self saveUserinfo];
}
- (void)setFamilyBuddy:(NSArray *)familyBuddy
{
    _familyBuddy = familyBuddy;
    [self saveUserinfo];
}
- (void)setCardList:(NSArray *)cardList
{
    _cardList = cardList;
    [self saveUserinfo];
}
- (void)setAllCash:(NSNumber *)allCash
{
    _allCash = allCash;
    [self saveUserinfo];
}

#pragma mark - 设置方法
- (void)setWithDictionary:(NSDictionary *)dictionary
{
    _username = dictionary[@"username"];            //tDictValue(dictionary[@"username"]);
    _sessionToken = dictionary[@"sessionToken"];    //tDictValue(dictionary[@"sessionToken"]);
    _objectId = dictionary[@"objectId"] ;           //tDictValue(dictionary[@"objectId"]);
    _familyName = dictionary[@"familyName"];        //tDictValue(dictionary[@"familyId"]);
    if (dictionary[@"cash"]) {
        _cash = dictionary[@"cash"];
    } else {
        _cash = [NSNumber numberWithInteger:0];
    }
//    _login = kimiNull(dictionary[@"_login"]);
    [self saveUserinfo];
}

- (void)login
{
    self.login = YES;
    [self upUserInfo];
}

- (void)upUserInfo
{
    [WalletNetwork findUserWithBlock:^(NSDictionary *dict) {
        [self setWithDictionary:dict];
        [self upBuddy];
        [self upAllCash];
    }];
}

- (void)upBuddy
{
    [WalletNetwork findBuddyWithBlock:^(NSArray *array) {
        self.familyBuddy = array;
        [self upCard];
    }];
}

- (void)upAllCash
{
    [WalletNetwork findBuddyAllCashWithBlock:^(NSNumber *allCash) {
        self.allCash = allCash;
    }];
}

- (void)upCard
{
    [WalletNetwork findCardListWithBlock:^(NSArray *array) {
        self.cardList = array;
    }];
}

@end
