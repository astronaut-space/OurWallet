//
//  UserInfoModel.h
//  OurWallet
//
//  Created by 小肥羊 on 15/12/28.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import <Foundation/Foundation.h>

#define USERINFO                ([UserInfoModel sharedManager])     //单例
//通知
#define nLoginChange            (@"LoginChangeNotice")      //登录改变
#define nUserInfoChange         (@"UserInfoChangeNotice")   //信息更改

@interface UserInfoModel : NSObject

@property (nonatomic, copy) NSString *objectId;             /**< 唯一识别标志 */
@property (nonatomic, copy) NSString *sessionToken;         /**< 会话令牌 */
@property (nonatomic, copy) NSString *username;             /**< 用户名 */
@property (nonatomic, copy) NSString *familyName;           /**< 组队名称 */
@property (nonatomic, copy) NSNumber *cash;                 /**< 现金 */
@property (nonatomic, getter=isLogin) BOOL login;           /**< 是否登录 */

@property (nonatomic, strong) NSArray *familyBuddy;         /**< 小伙伴 */
@property (nonatomic, strong) NSArray *cardList;            /**< 卡列表 */
@property (nonatomic, strong) NSNumber *allCash;            /**< 总现金 */

#pragma mark - 单例
+ (UserInfoModel *)sharedManager;

#pragma mark - 设置方法
- (void)setWithDictionary:(NSDictionary *)dictionary;

#pragma mark - 常用方法
/*! @brief 登录 */
- (void)login;
/*! @brief 刷新信息 */
- (void)upUserInfo;
/*! @brief 刷新小伙伴 */
- (void)upBuddy;
/*! @brief 刷新总现金 */
- (void)upAllCash;
/*! @brief 刷新卡 */
- (void)upCard;

@end
