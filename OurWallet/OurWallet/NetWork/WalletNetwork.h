//
//  WalletNetwork.h
//  OurWallet
//
//  Created by 小肥羊 on 15/12/29.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkDefine.h"

@interface WalletNetwork : NSObject

/*! @brief 注册 */
+ (void)registWithUserName:(NSString *)username WithPassword:(NSString *)password WithBlock:(void (^)(id object))block;
/*! @brief 登录 */
+ (void)loginWithUserName:(NSString *)username WithPassword:(NSString *)password WithBlock:(void (^)(id object))block;
/*! @brief 创建组 */
+ (void)createFamilyWithName:(NSString *)familyName WithBlock:(void (^)(id object))block;
/*! @brief 加入组 */
+ (void)joinFamilyWithName:(NSString *)familyName WithBlock:(void (^)(id object))block;
/*! @brief 离开组 */
+ (void)leaveFamilyWithBlock:(void (^)(id object))block;
/*! @brief 修改现金 */
+ (void)cashChangeWithMoney:(NSNumber *)money WithBlock:(void (^)(id object))block;
/*! @brief 创建卡 */
+ (void)createCardWithCardName:(NSString *)cardName WithMoney:(NSNumber *)money WithBlock:(void (^)(id object))block;

/*! @brief 拼接转换 */
+ (NSURL *)imageURLWithString:(NSString *)url;

@end
