//
//  WalletNetwork+UsingSDK.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/30.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "WalletNetwork+UsingSDK.h"

@implementation WalletNetwork (UsingSDK)

+ (void)findUserWithBlock:(void (^)(NSDictionary *dict))block
{
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"_User"];
    [bquery whereKey:@"objectId" equalTo:USERINFO.objectId];
    [bquery calcInBackgroundWithBlock:^(NSArray *array,NSError *error){
        for (NSDictionary *dict in array) {
            block(dict);
        }
    }];
}

+ (void)existsFamilyNameWith:(NSString *)familyName WithBlock:(void (^)(BOOL exists))block
{
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"Family"];
    [bquery whereKey:@"familyName" equalTo:familyName];
    [bquery countObjectsInBackgroundWithBlock:^(int number,NSError *error){
        block((number > 0));
    }];
}

+ (void)findBuddyWithBlock:(void (^)(NSArray *array))block
{
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"_User"];
    [bquery whereKey:@"familyName" equalTo:USERINFO.familyName];
    [bquery calcInBackgroundWithBlock:^(NSArray *array,NSError *error){
        block(array);
    }];
}

+ (void)findBuddyAllCashWithBlock:(void (^)(NSNumber *allCash))block
{
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"_User"];
    [bquery whereKey:@"familyName" equalTo:USERINFO.familyName];
    NSArray *sumArray = [NSArray arrayWithObject:@"cash"];
    [bquery sumKeys:sumArray];
    [bquery calcInBackgroundWithBlock:^(NSArray *array, NSError *error) {
        if (error) {
            NSLog(@"error is:%@",error);
        } else{
            if (array) {
                NSDictionary *dic = [[NSDictionary alloc] init];
                dic = [array objectAtIndex:0];
                block([dic objectForKey:@"_sumCash"]);
            }
        }
    }];
}

+ (void)checkFamilyOwnerWithBlock:(void (^)(BOOL isOwner))block
{
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"Family"];
    [bquery whereKey:@"familyName" equalTo:[UserInfoModel sharedManager].familyName];
    [bquery calcInBackgroundWithBlock:^(NSArray *array,NSError *error){
        for (NSDictionary *dict in array) {
            NSDictionary *owner = dict[@"owner"];
            if ([owner[@"objectId"] isEqualToString:[UserInfoModel sharedManager].objectId]) {
                block(YES);
                return;
            }
        }
        block(NO);
    }];
}

+ (void)findCardListWithBlock:(void (^)(NSArray *array))block
{
    BmobQuery *bquery = [BmobQuery queryWithClassName:@"Card"];
    if (USERINFO.familyName) {
        NSMutableArray *objectIdArray = [[NSMutableArray alloc] init];
        for (BmobObject *buddy in USERINFO.familyBuddy) {
            [objectIdArray addObject:[buddy objectForKey:@"objectId"]];
        }
        [bquery whereKey:@"userId" containedIn:objectIdArray];
    } else {
        [bquery whereKey:@"userId" equalTo:USERINFO.objectId];
    }
    [bquery calcInBackgroundWithBlock:^(NSArray *array,NSError *error){
        block(array);
    }];
}


#pragma mark - 记录
+ (void)changeRecordWithUseFor:(NSString *)useFor UseFrom:(NSString *)useFrom Money:(NSInteger)money Consume:(BOOL)consume Remark:(NSString *)remark RecordTime:(NSDate *)recordTime RecordImage:(BmobFile *)recordImage
{
    BmobObject *changeRecord = [BmobObject objectWithClassName:@"ChangeRecord"];
    [changeRecord setObject:USERINFO.objectId forKey:@"fromUserId"];
    [changeRecord setObject:useFor forKey:@"useFor"];
    [changeRecord setObject:useFrom forKey:@"useFrom"];
    [changeRecord setObject:[NSNumber numberWithInteger:money] forKey:@"money"];
    [changeRecord setObject:[NSNumber numberWithBool:consume] forKey:@"consume"];
    [changeRecord setObject:remark forKey:@"remark"];
    [changeRecord setObject:recordTime forKey:@"recordTime"];
    if (recordImage) {
        [changeRecord setObject:recordImage forKey:@"recordImage"];
    }
    [changeRecord saveInBackground];
}

@end
