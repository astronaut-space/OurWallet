//
//  WalletNetwork+UsingSDK.h
//  OurWallet
//
//  Created by 小肥羊 on 15/12/30.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "WalletNetwork.h"

@interface WalletNetwork (UsingSDK)

/*! @brief 查找用户 */
+ (void)findUserWithBlock:(void (^)(NSDictionary *dict))block;
/*! @brief 查找组名称是否存在 */
+ (void)existsFamilyNameWith:(NSString *)familyName WithBlock:(void (^)(BOOL exists))block;
/*! @brief 查找小伙伴 */
+ (void)findBuddyWithBlock:(void (^)(NSArray *array))block;
/*! @brief 小伙伴们总现金 */
+ (void)findBuddyAllCashWithBlock:(void (^)(NSNumber *allCash))block;
/*! @brief 是否是房主 */
+ (void)checkFamilyOwnerWithBlock:(void (^)(BOOL isOwner))block;
/*! @brief 查询卡列表 */
+ (void)findCardListWithBlock:(void (^)(NSArray *array))block;

/*! @brief 记录 */
+ (void)changeRecordWithUseFor:(NSString *)useFor UseFrom:(NSString *)useFrom Money:(NSInteger)money Consume:(BOOL)consume Remark:(NSString *)remark RecordTime:(NSDate *)recordTime RecordImage:(BmobFile *)recordImage;

@end
