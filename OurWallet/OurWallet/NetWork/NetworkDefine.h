//
//  NetworkDefine.h
//  OurWallet
//
//  Created by 小肥羊 on 15/12/29.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#ifndef NetworkDefine_h
#define NetworkDefine_h

#define Network_ErrorKey        (@"error")                  //错误

#define Network_Regist          (@"RegistWallet")           //注册
#define Network_Login           (@"LoginWallet")            //登录

#define Network_CreateFamily    (@"CreateFamily")           //创建组
#define Network_JoinFamily      (@"JoinFamily")             //加入组
#define Network_LeaveFamily     (@"LeaveFamily")            //离开组

#define Network_CashChange      (@"CashChange")             //修改现金

#define Network_CreateCard      (@"CreateCard")             //创建卡

#endif /* NetworkDefine_h */
