//
//  WalletNetwork.m
//  OurWallet
//
//  Created by 小肥羊 on 15/12/29.
//  Copyright © 2015年 小肥羊. All rights reserved.
//

#import "WalletNetwork.h"

@implementation WalletNetwork

+ (void)registWithUserName:(NSString *)username WithPassword:(NSString *)password WithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:username forKey:@"username"];
    [paras setValue:password forKey:@"password"];
    
    [BmobCloud callFunctionInBackground:Network_Regist withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (void)loginWithUserName:(NSString *)username WithPassword:(NSString *)password WithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:username forKey:@"username"];
    [paras setValue:password forKey:@"password"];
    
    [BmobCloud callFunctionInBackground:Network_Login withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (void)createFamilyWithName:(NSString *)familyName WithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:familyName forKey:@"familyName"];
    [paras setValue:USERINFO.objectId forKey:@"ownerId"];
    [paras setValue:USERINFO.sessionToken forKey:@"sessionToken"];
    
    [BmobCloud callFunctionInBackground:Network_CreateFamily withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (void)joinFamilyWithName:(NSString *)familyName WithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:familyName forKey:@"familyName"];
    [paras setValue:USERINFO.objectId forKey:@"ownerId"];
    [paras setValue:USERINFO.sessionToken forKey:@"sessionToken"];
    
    [BmobCloud callFunctionInBackground:Network_JoinFamily withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (void)leaveFamilyWithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:USERINFO.familyName forKey:@"familyName"];
    [paras setValue:USERINFO.objectId forKey:@"ownerId"];
    [paras setValue:USERINFO.sessionToken forKey:@"sessionToken"];
    
    [BmobCloud callFunctionInBackground:Network_LeaveFamily withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (void)cashChangeWithMoney:(NSNumber *)money WithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:USERINFO.objectId forKey:@"ownerId"];
    [paras setValue:USERINFO.sessionToken forKey:@"sessionToken"];
    [paras setValue:money forKey:@"money"];
    
    [BmobCloud callFunctionInBackground:Network_CashChange withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (void)createCardWithCardName:(NSString *)cardName WithMoney:(NSNumber *)money WithBlock:(void (^)(id object))block
{
    NSMutableDictionary *paras = [[NSMutableDictionary alloc] init];
    [paras setValue:cardName forKey:@"cardName"];
    [paras setValue:USERINFO.objectId forKey:@"ownerId"];
    [paras setValue:money forKey:@"money"];
    
    [BmobCloud callFunctionInBackground:Network_CreateCard withParameters:paras block:^(id object, NSError *error) {
        if (error) {
            block([NSDictionary dictionaryWithObject:error.domain forKey:Network_ErrorKey]);
        } else {
            block(object);
        }
    }] ;
}

+ (NSURL *)imageURLWithString:(NSString *)url
{
    if ([url rangeOfString:@"http"].location == NSNotFound) {
        url = [@"http://file.bmob.cn/" stringByAppendingString:url];
    }
    return [NSURL URLWithString:url];
}

@end
